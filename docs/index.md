![release](https://gitlab.com/symbiont/dipendency/-/badges/release.svg?key_text=version) ![pipeline](https://gitlab.com/symbiont/dipendency/badges/master/pipeline.svg?key_text=test)

---

# Dipendency

!!! warning
    This package is `work in progress`!

!!! Note
    This documentation only documents the technical usage of this package with little to no text.

Simple dependency library.

## Installation

Requires at least PHP `8.2`

```bash
composer require symbiont/dipendency
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: [https://symbiont.gitlab.io/dipendency](https://symbiont.gitlab.io/dipendency)
- Gitlab: [https://gitlab.com/symbiont/dipendency](https://gitlab.com/symbiont/dipendency)

---

## Testing

```php
composer test
```

## License

Released under MIT License