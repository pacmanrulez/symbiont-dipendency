# Symbiont / Dipendency

Simple dependency library.

This package is work in progress!

## Installation

Requires at least PHP `8.2`.

```bash
composer require symbiont/dipendency
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: [https://symbiont.gitlab.io/dipendency](https://symbiont.gitlab.io/dipendency)
- Gitlab: [https://gitlab.com/symbiont/dipendency](https://gitlab.com/symbiont/dipendency)

---

## Testing

```php
composer test
```

---

## License

[MIT license](LICENSE.MD)