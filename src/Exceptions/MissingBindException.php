<?php

namespace Symbiont\Dipendency\Exceptions;

class MissingBindException extends \Exception {

    public function __construct(...$args) {
        parent::__construct(sprintf('Binding `%s` does not exist', ...$args));
    }

}