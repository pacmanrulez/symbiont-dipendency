<?php

namespace Symbiont\Dipendency\Exceptions;

class ImmutableBindAttemptException extends \Exception {

    public function __construct(...$args) {
        parent::__construct(sprintf('Attempt to bind immutable `%s` failed', ...$args));
    }

}