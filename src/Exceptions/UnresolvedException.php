<?php

namespace Symbiont\Dipendency\Exceptions;

class UnresolvedException extends \Exception {

    public function __construct(...$args) {
        parent::__construct(sprintf('Unable to resolve `%s`', ...$args));
    }

}