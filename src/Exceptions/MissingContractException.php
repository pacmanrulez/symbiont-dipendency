<?php

namespace Symbiont\Dipendency\Exceptions;

class MissingContractException extends \Exception {

    public function __construct(...$args) {
        parent::__construct(sprintf('Missing contract, `%s` requires to implement `%s`', ...$args));
    }

}