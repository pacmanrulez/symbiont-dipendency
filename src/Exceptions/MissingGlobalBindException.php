<?php

namespace Symbiont\Dipendency\Exceptions;

class MissingGlobalBindException extends \Exception {

    public function __construct(...$args) {
        parent::__construct(sprintf('Binding `%s` does not exist in global scope', ...$args));
    }

}