<?php

namespace Symbiont\Dipendency\Concerns;

use Symbiont\Dipendency\Contracts\Dipendable;
use Symbiont\Dipendency\Exceptions\ImmutableBindAttemptException;

trait HandlesImmutables {

    protected array $immutables = [];

    public static $throw_exception_if_muted = false;

    public function bind(string $abstract, mixed $concrete, bool $immutable = false): Dipendable {
        return $this->bindImmutable($abstract, $concrete);
    }

    protected function bindImmutable(string $abstract, mixed $concrete): Dipendable {
        if($this->mutable($abstract)) {
            return $this->bindableBind($abstract, $concrete);
        }
        else {
            if (self::$throw_exception_if_muted) {
                throw new ImmutableBindAttemptException($abstract);
            }

            return $this;
        }

        return $this->immutable($abstract);
    }

    public function immutable(string $abstract): Dipendable {
        if($this->mutable($abstract)) {
            $this->immutables[] = $abstract;
        }

        return $this;
    }

    public function unmute(string $abstract): Dipendable {
        if(! $this->mutable($abstract)) {
            unset($this->immutables[$abstract]);
        }
        return $this;
    }

    public function mutable(string $abstract): bool {
        return ! in_array($abstract, $this->immutables);
    }

    public function isImmutable(string $abstract): bool {
        return ! $this->mutable($abstract);
    }

}