<?php

namespace Symbiont\Dipendency\Contracts;

interface Pendable {

    public static function __callStatic($name, array $args = []);

}