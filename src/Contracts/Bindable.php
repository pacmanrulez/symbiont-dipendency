<?php

namespace Symbiont\Dipendency\Contracts;

interface Bindable {

    public function bound(string $abstract): bool;
    public function bind(string $abstract, mixed $concrete): Dipendable;
    public function get(string $abstract, array $args = []);
    public function make(string $abstract, array $args = []);
    public function global(string $abstract, array $args = []);
    public function getBindings(): array;
    public function setBindings(array $bindings): Dipendable;

}