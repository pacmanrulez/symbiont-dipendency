<?php

namespace Symbiont\Dipendency\Contracts;

interface Immutable {

    public function bind(string $abstract, mixed $concrete, bool $immutable = false): Dipendable;
    public function bindableBind(string $abstract, mixed $concrete): Dipendable;
    public function immutable(string $abstract): Dipendable;
    public function unmute(string $abstract): Dipendable;
    public function mutable(string $abstract): bool;

}