<?php

namespace Symbiont\Dipendency;

use Symbiont\Dipendency\Contracts\{
    Dipendable, Bindable, Immutable
};
use Symbiont\Dipendency\Concerns\{
    HandlesBindables, HandlesImmutables
};

class Dipendency implements Dipendable, Bindable, Immutable {

    use HandlesBindables,
        HandlesImmutables {
            HandlesBindables::bind as bindableBind;
            HandlesImmutables::bind insteadof HandlesBindables;
    }

}
