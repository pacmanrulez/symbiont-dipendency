<?php

namespace Symbiont\Dipendency;

use Symbiont\Support\ForwardCall\ForwardsCall;
use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;

use Symbiont\Dipendency\Contracts\Dipendable;
use Symbiont\Dipendency\Contracts\Pendable;

/**
 * Simplest static DI
 * @method static bound(string $abstract): bool
 * @method static bind(string $abstract, mixed $concrete): self
 * @method static get(string $abstract, array $args = []): mixed
 * @method static make(string $abstract, array $args = []): mixed
 * @method static setBindings(): array
 * @method static getBindings(array $bindings): Symbiont\Dipendency\Contracts\Dipendable
 */
class Pendency implements Pendable, ForwardsCalls {

    use ForwardsCall;

    private static bool $initialized = false;

    protected static ?Dipendency $container = null;

    protected static function initialize() {
        if(! self::$initialized) {
            self::$container = new Dipendency;
            self::$container->ignore_global = true;
            self::$initialized = true;
        }
    }

    public static function forwardContainer(): array {
        static::initialize();

        return ['bound', 'bind', 'get', 'make', 'immutable', 'mutable', 'unmute', 'setBindings', 'getBindings'];
    }

    public static function getContainer(): Dipendable {
        static::initialize();

        return static::$container;
    }

}